var express = require('express'),
    app = express(),
    useragent = require('express-useragent');

const PORT = process.env.PORT || 3000;

app.enable('trust proxy');
app.use(useragent.express());

app.get('/', function(req, res) {
  res.send('<h1>Go to /api/whoami</h1>')
});

app.get('/api/whoami', function(req, res) {
  var t = [];
  var ip = req.ip;
  var os = req.useragent.os;
  var lang = req.headers["accept-language"].split(',')[0];
  res.jsonp({'ip': ip, 'os': os, 'lang': lang});
});

console.log('express server listening on port: ' + PORT);
app.listen(PORT);
